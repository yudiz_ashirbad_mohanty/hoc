
import { Route } from 'react-router-dom'
import { Switch } from 'react-router-dom'
import Error from '../Components/Error'
import Login from '../Components/Login'
import User from '../Components/User'
import PrivateRoute from './PrivateRoute'
import PublicRoute from './PublicRoute'
import Home from '../Components/Home'
import Portal from '../Components/Portal'

const Routes = (props) => {
  return (
    <div>
      <Switch>
        <PublicRoute  path='/' exact component={Login}/>
        <PrivateRoute path='/home'  component={Home}/>
        <PrivateRoute path='/user/:name' component={User}/>
        <PrivateRoute path='/portal' component={Portal}></PrivateRoute>
        <Route component={Error}/>
      </Switch>
    </div>
  )
}

export default Routes