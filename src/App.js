import React from 'react'
import Content from './Content'
import ErrorBoundary from './ErrorBoundary'


const App = () => {


  return (
    <div>
    <ErrorBoundary>
   <Content/>
  
   </ErrorBoundary>
    </div>
  )
}

export default App