import React, { useRef } from 'react'
import Login from './Login'


function Loginref() {
let inputRef =useRef(null)
function update (){
    inputRef.current.value='login'
}
  return (
    <div>
        <Login ref={inputRef}/>
        <button onClick={update}>click</button>
    </div>
  )
}

export default Loginref