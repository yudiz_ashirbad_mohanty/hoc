import React from 'react'
import './css/Navbar.css'
import { Link } from 'react-router-dom'
import { useHistory } from 'react-router-dom'
const Navbar = () => {
  let history =useHistory();
  const onlogout =()=>{
    localStorage.removeItem('token','value')
    history.push('/')
  }
  return (
    <div className='nav'>
    <ul>
        <li> <Link to='/home'>HOC</Link></li>
        <li> <Link to='/portal'>PORTAL</Link></li>
        <div>
        <button onClick={onlogout}>logout</button>
        </div>
    </ul>
     
    </div>
  )
}

export default Navbar