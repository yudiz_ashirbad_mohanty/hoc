import React, { Component } from 'react'
import Childportal from './Childportal'


export default class Parentportal extends Component {
  constructor(props){
    super(props);
    this.state={
      count:0,
    }
  }

  onIncrement = ()=>{
    this.setState({count:this.state.count+1});
  }
  render() {
    return (
      <div>
      COUNT THE NUMBER: {this.state.count}
       <Childportal increment={this.onIncrement}/>
      </div>
    )
  }
}
