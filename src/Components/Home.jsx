import React from 'react'
import Navbar from './Navbar'
import './css/Home.css'
import SearchTodos from "./TodoList";
import SearchUsers from "./UsersList";

const Home = () => {
  return (
    <div>
        <Navbar/>
        <div>
      <div className="section">
        <div>
          <SearchUsers />
        </div>
        <div>
          <SearchTodos />
        </div>
      </div>
        </div>
    </div>
  )
}

export default Home