import React, { Component } from 'react'
import ReactDOM from 'react-dom';
export default class Childportal extends Component {
  render() {
    return ReactDOM.createPortal(

      <button onClick={this.props.increment}>CLICK</button>,
      document.getElementById("portal")
    )
  }
}
