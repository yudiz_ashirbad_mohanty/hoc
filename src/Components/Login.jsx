import React from 'react';
import './css/Login.css'
import {forwardRef} from 'react'
import { useHistory } from 'react-router-dom'
const Login = (props,ref) => {
  let history =useHistory();
  const onSubmit =()=>{
    localStorage.setItem('token','value')
    history.push('/home')
  }
  return (
    <div className='login-main'>
    
    <div className='login-sub'>

        <h1>LOGIN</h1>
        <h3><label htmlFor='name'>EMAIL</label></h3>
        <div>
        <input ref={ref} id='name' type='email' placeholder='Your Email address' aria-label='name' aria-required='true'></input></div>
        <div>
          <h3><label htmlFor='password'>PASSWORD</label></h3>
          <div><input ref={ref} id='password' type='password' placeholder='Your password' aria-label='password' aria-required='true'></input></div>
        </div>
        <button onClick={onSubmit}>submit
        </button>
        </div>
      
    </div>
  )
}

export default forwardRef(Login)