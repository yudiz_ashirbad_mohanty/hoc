import React from 'react'
import { BrowserRouter as Router } from 'react-router-dom'

export default class Content extends React.Component{
  constructor(props){
    super(props)
    this.state={
      hasError:false
    }
    this.showError=this.showError.bind(this)
  }
  showError(){
    this.setState({hasError:true})
  }
  render(){
    if(this.state.hasError){
      throw new Error('App crashed now ')
    }
    const Routes =React.lazy(()=>import('./Routes/Routes'))
   return (
    <div>
    <React.Suspense fallback={<div>loading.... </div>}>
       <Router>
       <div>
         <button onClick={this.showError}>show error</button>
       </div>
       <Routes/>
    </Router>
    </React.Suspense>
    </div>
  )
  }
}


